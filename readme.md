
# Webcam Manager User Manual

Website and documentation for [Webcam Manager](https://wm.diagram.institute)

This website is built with [TiddlyWiki](https://tiddlywiki.com/), specifically the nodejs version used locally to produce a static site.

## Install 

1. Clone this repository

```bash
git clone https://codeberg.org/IDS/webcam_manager_userguide.git
```

2. Install TiddlyWiki

```bash
cd webcam_manager_userguide
npm install
``` 

3. Launch local server

```
tiddlywiki webcam-manager-user-guide --listen 
```

4. Open the address in a browser

```
syncer-server-filesystem: Dispatching 'save' task: $:/StoryList 
Serving on http://127.0.0.1:8080
```

## Modify the site

See the [TiddlyWiki documentation](https://tiddlywiki.com/)

To add a page:
- Click the "+" button in the sidebar
- Assign the section with the **tag name** menu under the title
- Choose between TiddlyWiki syntax (default) or Markdown using the "type" menu at the bottom

Images are added by dragging them onto the wiki browser window.

To contribute your changes back to the main site please [submit a pull-request](https://codeberg.org/IDS/webcam_manager_userguide/pulls).

## Build as a static site 

```bash
cd webcam_manager_userguide
./build.sh 
```

This produces HTML files and static assets in `output/static/` (ignored by git).
 