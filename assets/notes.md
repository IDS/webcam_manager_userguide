

## Adding a webfont

Embed webfont in css: 

1. upload files to https://transfonter.org/
2. choose Base64 encode
3. download .zip
4. check the font-family name, and copy/paste content of css in a tiddler tagged `$:/tags/Stylesheet`