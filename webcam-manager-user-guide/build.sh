#!/bin/bash

rm -rf output/*

# https://rpdillon.net/how-to-use-tiddlywiki-as-a-static-site-generator.html

echo -n "Rendering pages..."
tiddlywiki --render '[!is[system]]' "[is[tiddler]addsuffix[.html]slugify[]]" text/plain $:/core/templates/static.tiddler.html
echo "done."

# tiddlywiki --rendertiddler ["User Manual"] $:/core/templates/static.tiddler.html index.html text/plain
tiddlywiki --rendertiddler $:/core/templates/static.template.css static.css text/plain

mv output/user-manual.html output/index.html

# XXX find a better way to copy static files
# I think this has part of the solution: https://tiddlywiki.com/#ExternalImages
cp tiddlers/\$__favicon.ico output/favicon.ico
cp tiddlers/speed_loop.mp4 output/speed_loop.mp4
cp tiddlers/nice_hat.mp4 output/nice_hat.mp4
cp tiddlers/app_icon_black.png output/app_icon_black.png
cp tiddlers/IDS_logo_sphere_white.svg output/IDS_logo_sphere_white.svg
cp tiddlers/banner_opengraph.png output/banner_opengraph.png