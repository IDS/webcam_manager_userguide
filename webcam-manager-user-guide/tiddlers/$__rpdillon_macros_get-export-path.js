(function(){
"use strict";

exports.name = "tv-get-export-path";

exports.params = [
    {title: ""}
];

/*
Run the macro
*/
exports.run = function(title) {
    var sanitized_title = this.wiki.slugify(title);
    return ("./"+ sanitized_title).toLocaleLowerCase();
}
})();