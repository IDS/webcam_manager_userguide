See [Camera Spoofing on tvtropes.org](https://tvtropes.org/pmwiki/pmwiki.php/Main/CameraSpoofing)

> In order to fool a particularly inept [Mook](https://tvtropes.org/pmwiki/pmwiki.php/Main/Mooks), our heroes must covertly make themselves invisible to a security  camera. They replace the camera's view with an image of the same scene,  so the ever-watchful officer will never notice.

> There are two basic variants:

> **The Polaroid Punk** — The heroes take an instant  photograph from the camera's perspective and then place it in front of  the camera. This may or may not require blocking or removing the camera  in the process, but even if it does, the security officer on duty will [dismiss it as just a temporary glitch](https://tvtropes.org/pmwiki/pmwiki.php/Main/TheGuardsMustBeCrazy), because the image is back, showing no activity, before they can  investigate. Whether or not (or exactly how long) this works depends on a number of factors, such as whether the security camera is capable of  focusing that close to its own lens (otherwise the photo will be  horribly blurred out) how well-illuminated the picture is (especially  compared to the scene it's imitating), and whether or not there's any  ambient movement (such as outside traffic or a flowing fountain) that  normally *should* be in the scene — because the guard might catch on that the image is *too* still to be a live feed.

> **The Splice and Dice** — The tech pro on the team manages (by means of infiltrating and/or [hacking](https://tvtropes.org/pmwiki/pmwiki.php/Main/EverythingIsOnline)) to capture some amount of live camera footage, then feeds the recorded  footage to the security monitors in an endless loop. This avoids some of the problems posed by taking a still photograph, but if the footage  contains any activity (as in the case of *[Speed](https://tvtropes.org/pmwiki/pmwiki.php/Film/Speed)*) the guard may eventually notice that the same activity is looping over and over and catch on.

> Note that it doesn't matter if the camera is a rotating one: It  just makes it harder, but eventually the tech guy will sync the loop  with the camera's movement, or the photo guy will put a curved panoramic shot in front of it.

> In either case, the deception will inevitably break down, alerting the guards to the prank at some dramatic moment.