*Webcam Manager* is designed to be used on devices that may be monitored, for example through desktop surveillance (screenshots taken at random intervals).

For this reason, nothing in the program window explicitly refers to looping the webcam. The application window looks as generic as possible. The few buttons are presented with no text and only icons usually associated with camera focus. This means that *Webcam Manager* may take some practice to operate. Fortunately, recording and playing webcam loops are the only functions available so the program is easy to learn. 

![User-Interface Guide](UI-guide.png)  

## Connect to call software 

1. Launch *Webcam Manager*
2. Ensure the webcam is showing in the preview
3. Launch the video-call software
4. Select `Dummy video device` as a camera 
   - In browser-based video-calls (Jitsi, Google Meet, Zoom) this will be associated with the dialog asking for "permission" to use the camera. See example below in Jitsi and Firefox.
   - In applications (Zoom, Microsoft Teams, etc) this is more likely to be found in video settings.

![example camera source dialog in Jitsi and Firefox with "Dummy video device" selected.](video-device.png)

## Recording a loop

1. Place your mouse cursor over the REC button
2. Click to start recording 
3. Keep a static pose moving as little as possible
4. Click again to stop recording 

## Playing a loop 

Once a loop has been recorded.

1. Click the LOOP button or press the `Enter` key 
2. The loop button is yellow when a loop is playing 
3. Click the STOP button or press `Enter` to stop and display the live webcam again.

## Freezing the camera 

To make the transition from loop back to a live camera, it can be useful to "freeze" the image and blame it on the connection.

Opening the About window with the **?** button freezes the picture for as long as the window is open.
