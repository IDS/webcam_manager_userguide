// Both get-export-link.js and get-export-path.js 
// are taken from
// https://www.didaxy.com/exporting-static-sites-from-tiddlywiki-part-5
// Those macros are updated here to use 
// this.wiki.slugify, which is built-in
// and produces better output than a simple 
// replace.  This behavior is coordinated with the
// build script, which runs tiddler URLs through
// slugify when exporting them.
(function(){
"use strict";

exports.name = "tv-get-export-link";

exports.params = [];

exports.run = function() {
    var title = this.to;
    var sanitized_title = this.wiki.slugify(title);
    var attr = this.getVariable("tv-subfolder-links");
    var path_to_root="./"	
    var finalLink=path_to_root

    
    var wikiLinkTemplateMacro = this.getVariable("tv-wikilink-template"),
        wikiLinkTemplate = wikiLinkTemplateMacro ? wikiLinkTemplateMacro.trim() : "#$uri_encoded$",
        wikiLinkText = wikiLinkTemplate.replace("$uri_encoded$",encodeURIComponent(sanitized_title));	
    wikiLinkText = wikiLinkText.replace("$uri_doubleencoded$",encodeURIComponent(sanitized_title));
    return (finalLink + wikiLinkText).toLocaleLowerCase();
};

})();