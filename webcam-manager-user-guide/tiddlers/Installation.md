The current version is `0.1_alpha`

## Windows

On Windows *Webcam Manager* relies on [OBS Studio](https://obsproject.com/) to create a virtual webcam that can be used in video-call software.

📦 [Webcam Manager Installer](https://codeberg.org/IDS/webcam_manager/raw/tag/0.1_alpha/py/dist/WebcamManager_installer.exe)

Installs both *Webcam Manager* and *OBS Studio* 

📦 [webcam_manager.exe](https://codeberg.org/IDS/webcam_manager/raw/tag/0.1_alpha/py/dist/webcam_manager.exe)

Only *Webcam Manager* if OBS is already installed.

## MacOS and Linux 

Apps are not yet available. Please follow the [development installation](https://codeberg.org/IDS/webcam_manager#development-installation).
